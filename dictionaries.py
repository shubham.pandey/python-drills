def word_count(s):
    """
    Find the number of occurrences of each word
    in a string(Don't consider punctuation characters)
    """
    l = s.split()
    x = dict()
    for item in l:
        if item in x:
            x[item] += 1
        else:
            x[item] = 1

    return(x)


def dict_items(d):
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    """
    list_ = []
    for (key, value) in d.items():
        list_.append((key, value))
    return list_


def dict_items_sorted(d):
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    sorted by `d`'s keys
    """
    dnew = []
    for key in sorted(d.keys()):
        dnew.append((f'{key}', d[key]))
    return dnew
