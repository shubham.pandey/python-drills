def integers_from_start_to_end_using_range(start, end, step):
    """return a list"""
    newList = [i for i in range(start, end, step)]
    return newList


def integers_from_start_to_end_using_while(start, end, step):
    """return a list"""
    i = start
    l = []
    while i in range(start, end, step):
        l.append(i)
        i += step
    return l


def two_digit_primes():
    """
    Return a list of all two-digit-primes
    """

    list_ = []
    for number in range(10, 100):

        flag = 0

        for div in range(2, int(number**(0.5))+1):

            if number % div == 0:
                flag = 1
                break
        if flag == 0:
            list_.append(number)

    return list_
