def html_dict_search(html_dict, selector):
    list_ = []
    selector = selector[1:]

    def dict_data(dict, selector):
        for key in dict:
            if (key == "attrs"):
                for attrskeys in dict["attrs"]:
                    if "class" in dict["attrs"]:
                        if (dict["attrs"]["class"] == selector):
                            list_.append(dict)
                    if "id" in dict["attrs"]:
                        if (dict["attrs"]["id"] == selector):
                            list_.append(dict)
            if(type(dict[key]) == list):
                for item in dict[key]:
                    dict_data(item, selector)
    dict_data(html_dict, selector)
    return list_
