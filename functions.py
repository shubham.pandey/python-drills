def is_prime(n):
    """
    Check whether a number is prime or not
    """
    flag = 0
    for i in range(2, int(n**0.5)+1):
        if n % i == 0:
            flag = 1
            break
    return (True if flag == 0 else False)


def n_digit_primes(digit=2):
    """
    Return a list of `n_digit` primes using the `is_prime` function.

    Set the default value of the `digit` argument to 2
    """
    list_ = []
    if digit == 1:
        for number in range(2, 10):
            flag = 0
            for divisor in range(2, int(number**0.5)+1):
                if number % divisor == 0:
                    flag = 1
                    break
            if flag == 0:
                list_.append(number)
    else:
        for number in range(10, 100):
            flag = 0
            for divisor in range(2, int(number**0.5)+1):
                if number % divisor == 0:
                    flag = 1
                    break
            if flag == 0:
                list_.append(number)
    return list_
