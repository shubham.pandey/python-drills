"""
* Create a class called `Point` which has two instance variables,
`x` and `y` that represent the `x` and `y` co-ordinates respectively. 

* Initialize these instance variables in the `__init__` method

* Define a method, `distance` on `Point` which accepts another `Point` object as an argument and 
returns the distance between the two points.
"""


class Point:
    def __init__(self, x_co, y_co):
        self.x = x_co
        self.y = y_co

    def distance(self, Point):
        distance_ = ((Point.x-self.x)**2+(Point.y-self.y)**2)**0.5
        return distance_
