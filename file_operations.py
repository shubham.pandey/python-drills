"""
Implement the below file operations.
Close the file after each operation.
"""


def read_file(path):
    fileName = open(path)
    f = fileName.read()
    fileName.close()
    return f


def write_to_file(path, s):
    f = open(path, "w")
    f.write(s)
    f.close()


def append_to_file(path, s):
    f_append = open(path, "a")
    f_append.write(s)
    f_append.close()


def numbers_and_squares(n, file_path):
    """
    Save the first `n` natural numbers and their squares into a file in the csv format.

    Example file content for `n=3`:

    1,1
    2,4
    3,9
    """
    file_ = open(file_path, "w")
    for i in range(1, n+1):
        file_.write(f'{i},{i**2}\n')
    file_.close()
